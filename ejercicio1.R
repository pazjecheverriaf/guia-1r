vector1 <- sample (1:120, 120, replace = TRUE)
vector1
vector2 <- 1:12

vector2 <- c(mean (vector1[1:10]) , mean (vector1[11:20]), mean (vector1[21:30]), 
             mean (vector1[31:40]), mean (vector1[41:50]), mean (vector1[51:60]),
             mean (vector1[61:70]),  mean (vector1[71:80]), mean (vector1[81:90]),
             mean (vector1[91:100]), mean (vector1[101:110]), mean (vector1[111:120]) )
                                                                
vector2 

meses <- c("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
           "Julio", "Agosto", "Semptiembre", "Octubre", "Noviembre",
           "Diciembre")

names(vector2)<-meses
vector2

sprintf (" El mes con el mayor promedio es : %s ", max(vector2))
sprintf (" EL mes con el menor promedio es : %s", min(vector2))

sprintf (" La suma de todos los elementos es : %s", sum(vector1))

sprintf (" posicion del elemento mayor del vector 1 es : %s", which.max(vector1))
sprintf (" posicion del elemento mayor del vector 2 es : %s", which.max(vector2))

sprintf (" posicion del elemento menor del vector 1 es : %s", which.min(vector1))
sprintf (" posicion del elemento menor del vector 2 es : %s", which.min(vector2))

sprintf (" La raiz cuadrada de la suma del vector 1 es : %f", sqrt(sum(vector1)))
sprintf (" La raiz cuadrada de la suma del vector 2 es : %f", sqrt(sum(vector2)))


 